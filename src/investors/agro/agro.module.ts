import { Module } from '@nestjs/common';
import { AgroService } from './agro.service';
import { MongooseModule } from '@nestjs/mongoose';
import {
  AgroInvestorsCertficate,
  AgroInvestorsCertficateSchema,
} from './schema/Agro.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: AgroInvestorsCertficate.name,
        schema: AgroInvestorsCertficateSchema,
      },
    ]),
  ],
  providers: [AgroService],
  exports: [AgroService],
})
export class AgroModule {}
