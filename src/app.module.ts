import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { InvestorsModule } from './investors/investors.module';
import { AnnualSummitModule } from './annual-summit/annual-summit.module';
import { EmailStatusModule } from './email_status/email_status.module';
import { VoteModule } from './summit/vote/vote.module';
import { ShareholderModule } from './summit/shareholder/shareholder.module';
import { PdfModule } from './pdf/pdf.module';
import { kegeberewTowerModule } from './kegeberewTower/kegeberewTower.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
console.log(process.env.MONGODB_URL);
@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MongooseModule.forRoot(process.env.MONGODB_URL),
    InvestorsModule,
    AnnualSummitModule,
    EmailStatusModule,
    ShareholderModule,
    // AuthModule,
    // AgendaModule,
    // LogModule,
    VoteModule,
    PdfModule,
    kegeberewTowerModule,
    UsersModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
