import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type RepresentativeDocument = Document & RepresentativePerson;

@Schema()
export class RepresentativePerson {
  @Prop()
  firstName: string;

  @Prop()
  middelName: string;

  @Prop()
  lastName: string;

  @Prop()
  address: string;

  @Prop()
  city: string;

  @Prop()
  country: string;

  @Prop()
  houseno: string;

  @Prop()
  RfirstName: string;

  @Prop()
  RmiddelName: string;

  @Prop()
  RlastName: string;

  @Prop()
  Raddress: string;

  @Prop()
  Rcity: string;

  @Prop()
  Rcountry: string;

  @Prop()
  Rhouseno: string;

  @Prop()
  share_no: string;

  @Prop()
  share_amount: string;

  @Prop()
  share_date: string;
}

export const RepresentativeSchema =
  SchemaFactory.createForClass(RepresentativePerson);
