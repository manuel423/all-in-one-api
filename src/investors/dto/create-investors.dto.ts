import { Type } from 'class-transformer';
import {
  IsEmail,
  IsNumber,
  ValidateNested,
  IsString,
  IsNotEmpty,
  IsOptional,
  IsIn,
} from 'class-validator';
import { ShareTypeEnum } from '../ShareTypeEnum';

class fullName {
  @IsString()
  firstName: string;

  @IsString()
  @IsOptional()
  middleName: string;

  @IsString()
  @IsOptional()
  lastName: string;

  @IsString()
  @IsOptional()
  title: string;
}

class fullNameAmharic {
  @IsString()
  amharicFirstName: string;

  @IsString()
  @IsOptional()
  amharicMiddleName: string;

  @IsString()
  @IsOptional()
  amharicLastName: string;

  @IsString()
  @IsOptional()
  title: string;
}

export class CreateInvestorsDto {
  @IsString()
  @IsNotEmpty()
  serialNumber: string;

  @IsString()
  @IsNotEmpty()
  accountNumber: string;

  // @IsString()
  // fullName: string;

  // @IsString()
  // fullNameAmharic: string;
  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => fullName)
  fullName: fullName;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => fullNameAmharic)
  fullNameAmharic: fullNameAmharic;

  @IsNumber()
  @IsNotEmpty()
  numberofShares: number;

  @IsNumber()
  @IsNotEmpty()
  amountPaid: number;

  @IsNumber()
  @IsNotEmpty()
  from: string;

  @IsNumber()
  @IsNotEmpty()
  to: string;

  // @IsNumber()
  @IsNotEmpty()
  certficateNumber: string;

  @IsString()
  @IsOptional()
  telephone: string;

  @IsEmail()
  @IsOptional()
  email: string;

  @IsOptional()
  @IsString()
  status: string;

  @IsIn([
    ShareTypeEnum.franchise,
    ShareTypeEnum['kegeberew tower'],
    ShareTypeEnum.kipt,
    ShareTypeEnum.ordinary,
    ShareTypeEnum.others,
  ])
  @IsOptional()
  type: string;

  @IsOptional()
  @IsString()
  sheetName: string;

  @IsOptional()
  remark: string;

  @IsString()
  @IsOptional()
  enumName: string;

  @IsOptional()
  location: string;

  @IsOptional()
  @IsString()
  moneyToWordEng: string;
  
  @IsOptional()
  @IsString()
  moneyToWordETH: string;

  @IsOptional()
  @IsString()
  dateFormatEng: string;
  
  @IsOptional()
  @IsString()
  dateFormatETH: string;

}
