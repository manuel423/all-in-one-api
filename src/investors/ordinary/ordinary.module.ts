import { Module } from '@nestjs/common';
import { OrdinaryService } from './ordinary.service';
import { MongooseModule } from '@nestjs/mongoose';
import {
  OrdinaryInvestorsCertficate,
  OrdinaryInvestorsCertficateSchema,
} from './schema/Ordinary.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: OrdinaryInvestorsCertficate.name,
        schema: OrdinaryInvestorsCertficateSchema,
      },
    ]),
  ],
  providers: [OrdinaryService],
  exports: [OrdinaryService],
})
export class OrdinaryModule {}
