import { IsNumber, IsOptional, IsString } from 'class-validator';

export class FilterInvestorDto {
  @IsOptional()
  @IsNumber()
  serialNumber: number;

  @IsOptional()
  @IsNumber()
  accountNumber: number;

  @IsOptional()
  @IsString()
  email: string;

  @IsOptional()
  @IsString()
  name: string;
}
