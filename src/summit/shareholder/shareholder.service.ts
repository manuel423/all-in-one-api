/* eslint-disable prettier/prettier */
/* eslint-disable no-var */
/* eslint-disable prettier/prettier */
import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { verify } from 'jsonwebtoken';
import { Shareholder, ShareholderDocument } from './schema/shareholder.schema';
import { CreateShareholderDto } from './dto/create-shareholder.dto';
import { UpdateShareholderDto } from './dto/update-shareholder.dto';

import { Model } from 'mongoose';
// import {Workbook} from 'exceljs'
import { resolve } from 'path';
import * as tmp from 'tmp';
import { tmpdir } from 'os';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import axios from 'axios';
@Injectable()
export class ShareholderService {
  // UserLogger = new Logger(UserService.name);

  constructor(
    @InjectModel(Shareholder.name)
    private shareholderModel: Model<ShareholderDocument>,
  ) {}
  async createshareholder(user: CreateShareholderDto) {
    try {
      console.log(user);
      var createshareholder = await this.shareholderModel.create(user);
      if (createshareholder) {
        return {
          data: createshareholder,
          message: 'share holder create successfully',
        };
      } else {
        return {
          data: createshareholder,
          message: 'share holder not created ',
        };
      }
    } catch (error) {
      console.log(error);
    }
  }
  async getuserbyid(id) {
    try {
      return await this.shareholderModel.findOne({ _id: id });
    } catch (error) {
      console.log(error);
    }
  }
  async updateuser(_id, updateUser: UpdateShareholderDto) {
    try {
      return await this.shareholderModel.findByIdAndUpdate(_id, updateUser);
    } catch (error) {
      console.log(error);
    }
  }
  async getall(documentsToSkip = 0, limitOfDocuments?: number) {
    const findQuery = this.shareholderModel
      .find()
      .sort({ _id: 1 })
      .skip(documentsToSkip);

    if (limitOfDocuments) {
      findQuery.limit(limitOfDocuments);
    }
    const results = await findQuery;
    const count = await this.shareholderModel.count();

    return { results, count, message: 'user created successfully' };
  }
  async filterlength() {
    try {
      return await this.shareholderModel.aggregate([
        {
          $match: {
            status: true,
          },
        },
        {
          $group: {
            _id: '$status',
            fieldN: {
              $sum: '$number0fshares',
            },
            num: {
              $sum: '$amountSubscribed',
            },
            numw: {
              $sum: 1,
            },
          },
        },
      ]);
    } catch (error) {
      console.log(error);
    }
  }
  async getshareholderbysequencenumber(_seq) {
    try {
      return this.shareholderModel.findOne({ sequesnce_num: _seq });
    } catch (error) {
      console.log(error);
    }
  }

  // get excel file
  async getFileFromExcel() {
    await axios
      .get('http://localhost:4000/excel/farmer')
      .then((res) => {
        console.log(res.data);

        res.data.forEach((element) => {
          let p: CreateShareholderDto = {
            fullName: element.fullName,
            gender: element.gender || '',
            email: element.fullNameAmharic,
            phoneNumber: element.phoneNumber || '',
            optionalPhone: element.optionalPhone || '',
            premiumCollected: element.premiumCollected || '',
            number0fshares: element.totalShareAmount,
            amountSubscribed: element.totalShareAmountBirr,
            amountPaidincash: element.amountPaidincash || '',
            type: element.shareType,
            regestrationdate: element.regestrationdate || '',
            status: element.status || false,
            sequesnce_num: element.serialNo,
          };
          console.log('sharehodler creating....')
          this.createshareholder(p);
        });
      })
      .catch((err) => console.log(err));
  }
}
