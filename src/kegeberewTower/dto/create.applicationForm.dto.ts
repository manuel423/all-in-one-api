import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateKegeberewTowerPerson {
  @IsString()
  first_name: string;

  @IsString()
  middel_name: string;

  @IsString()
  @IsOptional()
  last_name: string;

  @IsString()
  address: string;

  @IsString()
  kebele: string;

  @IsString()
  telephone: string;

  @IsString()
  telephone_alt: string;

  @IsString()
  house_no: string;

  @IsString()
  bank_account: string;

  @IsString()
  Rfirst_name: string;

  @IsString()
  @IsOptional()
  Rmiddel_name: string;

  @IsString()
  @IsNotEmpty()
  Rlast_name: string;

  @IsString()
  Raddress: string;

  @IsString()
  Rkebele: string;

  @IsString()
  Rtelephone: string;

  @IsString()
  Rtelephone_alt: string;

  @IsString()
  Rhouse_no: string;

  @IsString()
  kegeberewTower_type: string;

  @IsString()
  applicant_name: string;

  @IsNumber()
  // @IsNotEmpty()
  share_no: number;

  @IsNumber()
  share_amount: number;

  @IsString()
  share_date: string;
}
