import { Controller, Param, UsePipes, ValidationPipe } from '@nestjs/common';
import { Body, Get, Post, Put, Query } from '@nestjs/common/decorators';
import { CreateInvestorsDto } from './dto/create-investors.dto';
import { OrdinaryService } from './ordinary/ordinary.service';
import { AgriService } from './agri/agri.service';
import { KiptService } from './kipt/kipt.service';
import { UpdateInvestorDto } from './dto/update-investors.dto';
import { AgroService } from './agro/agro.service';
import { FilterInvestorDto } from './dto/filter.dto';

@Controller('investors')
export class InvestorsController {
  constructor(
    private investorService: OrdinaryService,
    private agriService: AgriService,
    private farmService: KiptService,
    private readonly agroService: AgroService,
  ) {}

  //===================================ORDINARY===================================
  @Post('ordinary/old')
  getAllDataFromExcel(@Body() data) {
    return this.investorService.getExcelData(data);
  }

  @Get('ordinary/statstic')
  ordinaryData() {
    return this.investorService.stasticOrdinaryInvestorsData();
  }

  @Get('ordinary/all')
  getAllData() {
    return this.investorService.findAllInvestors();
  }

  @Get('ordinary/:id')
  async getShareholderData(@Param('id') id: string) {
    return await this.investorService.findOneShareholder(id);
  }

  @Get('ordinary/generate/certficate')
  generateCertificate() {
    return this.investorService.generateCertifcateID();
  }

  @Get('ordinary/filter/certificate')
  filterData(@Query('type') type: string) {
    console.log(type);
    return this.investorService.filterInvestors(type);
  }

  @Get('ordinary/statstic/all')
  getStatsticDatas() {
    return this.investorService.dashboardStatic();
  }

  @Get('ordinary/filter')
  filterOrdinary(@Query() _filterDto: FilterInvestorDto) {
    return this.investorService.filterOrdinary(_filterDto);
  }

  @Get('ordinary/statstic/percentage')
  getStatsticDatasPercentage() {
    return this.investorService.dashboardStaticPercentage();
  }

  @Post('ordinary/create')
  @UsePipes(ValidationPipe)
  createNewInvestors(@Body() _investors: CreateInvestorsDto) {
    console.log('ordinary create ...');
    return this.investorService.createInvestors(_investors);
  }

  @Put('ordinary/update/:id')
  // @UsePipes(ValidationPipe)
  updateOrdinaryInvestors(
    @Param('id') id: string,
    @Body() _investors: UpdateInvestorDto,
  ) {
    console.log('Ordinary update ...');
    return this.investorService.updateOrdinaryById(id, _investors);
  }

  // ========================================AGRI===========================
  @Post('agriculture/old')
  getAllAgriDataFromExcel(@Body() data) {
    return this.agriService.getAgriExcelData(data);
  }

  @Get('agriculture/statstic')
  agriDatas() {
    return this.agriService.stasticAAgriInvestorsData();
  }

  @Get('agriculture/filter')
  filterAgri(@Query() _filterDto: FilterInvestorDto) {
    return this.agriService.filterAgriculture(_filterDto);
  }

  @Get('agriculture/all')
  getAllAgriData() {
    return this.agriService.findAAgriInvestors();
  }

  @Get('agriculture/:id')
  async getAgriShareholderData(@Param('id') id: string) {
    return await this.agriService.findOneAgriShareholder(id);
  }

  @Get('agriculture/generate/certficate')
  generateAgriCertificate() {
    return this.agriService.generateAgriCertifcateID();
  }

  @Post('agriculture/create')
  // @UsePipes(ValidationPipe)
  createNewAgInvestors(@Body() _investors: CreateInvestorsDto) {
    console.log('Agri create ...');
    return this.agriService.createInvestors(_investors);
  }

  @Put('agriculture/update/:id')
  // @UsePipes(ValidationPipe)
  updateAgriInvestors(
    @Param('id') id: string,
    @Body() _investors: UpdateInvestorDto,
  ) {
    console.log('Agriculture update ...');
    return this.agriService.updateAgriById(id, _investors);
  }

  // ========================================AGRO===========================
  @Post('agro/old')
  getAllAgroDataFromExcel(@Body() data) {
    return this.agroService.getAgroExcelData(data);
  }

  @Get('agro/statstic')
  agroDatas() {
    return this.agroService.stasticAgroInvestorsData();
  }

  @Get('agro/filter')
  filterAgro(@Query() _filterDto: FilterInvestorDto) {
    return this.agroService.filterAgro(_filterDto);
  }

  @Get('agro/all')
  getAllAgroData() {
    return this.agroService.findAAgroInvestors();
  }

  @Get('agro/:id')
  async getAgroShareholderData(@Param('id') id: string) {
    return await this.agroService.findOneAgroShareholder(id);
  }

  @Get('agro/generate/certficate')
  generateAgroCertificate() {
    return this.agroService.generateAgroCertifcateID();
  }

  @Post('agro/create')
  // @UsePipes(ValidationPipe)
  createNewAgroInvestors(@Body() _investors: CreateInvestorsDto) {
    console.log('Agri create ...');
    return this.agroService.createInvestors(_investors);
  }

  @Put('agro/update/:id')
  // @UsePipes(ValidationPipe)
  updateAgroInvestors(
    @Param('id') id: string,
    @Body() _investors: UpdateInvestorDto,
  ) {
    console.log('Agro-processing update ...');
    return this.agroService.updateAgroById(id, _investors);
  }

  //==================================END AGRO======================

  //=========================================KIPT======================
  @Get('farmers/old')
  getAllFarmerDataFromExcel() {
    return this.farmService.getFarmersExcelData();
  }

  @Get('farmers/statstic')
  farmersData() {
    return this.farmService.stasticFarmersInvestorsData();
  }

  @Get('farmers/all')
  getSingleFarmeriData() {
    return this.farmService.getAllFarmers();
  }

  @Get('farmers/filter')
  filterFarmer(@Query() _filterDto: FilterInvestorDto) {
    return this.farmService.FilterFarmers(_filterDto);
  }

  @Get('farmers/:id')
  async getFarmerShareholderData(@Param('id') id: string) {
    return await this.farmService.singleFarmer(id);
  }

  @Post('farmers/create')
  @UsePipes(ValidationPipe)
  createNewFarmerInvestors(@Body() _investors: CreateInvestorsDto) {
    console.log('Farmer create ...');
    return this.farmService.createInvestors(_investors);
  }

  @Put('farmers/update/:id')
  // @UsePipes(ValidationPipe)
  updateFarmerInvestors(
    @Param('id') id: string,
    @Body() _investors: UpdateInvestorDto,
  ) {
    console.log('Farmer update ...');
    return this.farmService.updateFarmerById(id, _investors);
  }

  @Get('farmers/generate/certficate')
  generateFarmersCertificate() {
    return this.farmService.generateCertifcateID();
  }

  //=================================END KIPT===================

  /**
   * @description filter serialNumber, accountNumber, email and name from ordinary, agriculture, agro and kipt shasreholders
   * @param filterDto
   * @returns
   */
  @Get('filter/all')
  async filterAll(@Query() filterDto: FilterInvestorDto) {
    let o = await this.investorService.filterOrdinary(filterDto);
    let f = await this.farmService.FilterFarmers(filterDto);
    let agri = await this.agriService.filterAgriculture(filterDto);
    let agro = await this.agroService.filterAgro(filterDto);

    const _result = [];

    _result.push(...o, ...f, ...agri, ...agro);

    return _result;
  }

  @Get('dashboard/statstic/piechart')
  async dashboardStaticPercentage() {
    let _totalORDINARY = await (
      await this.investorService.findAllInvestors()
    ).length;
    let _totalAGRI = await (await this.agriService.findAAgriInvestors()).length;
    let _totalAGRO = await (await this.agroService.findAAgroInvestors()).length;
    let _totalFRMERS = await (await this.farmService.getAllFarmers()).length;
    // let _totalORDINARY = (await (await this.investorService.findAllInvestors()).length);

    let _total = _totalAGRI + _totalAGRO + _totalFRMERS + _totalORDINARY;

    // let _resultPercent = {
    //   ordinary: (_totalORDINARY / _total) * 100,
    //   agriculture: (_totalAGRI / _total) * 100,
    //   farmers: (_totalFRMERS / _total) * 100,
    //   'agro-processing': (_totalAGRO / _total) * 100,
    // };
    let _resultPercent = {
      ordinary: _totalORDINARY,
      agriculture: _totalAGRI,
      farmers: _totalFRMERS,
      'agro-processing': _totalAGRO,
    };

    let totalData = {
      ordinary: _totalORDINARY,
      agriculture: _totalAGRI,
      farmers: _totalFRMERS,
      'agro-processing': _totalAGRO,
    };

    return {
      dashboard_percentage: {
        data: Object.values(_resultPercent),
        label: Object.keys(_resultPercent),
      },
      totalData,
    };
  }
}
