export default {
  db: {
    user: null,
    pass: null,
    host: 'localhost',
    port: '27017',
    database: 'testdb',
    authSource: null,
  },
  host: {
    url: 'localhost',
    port: '5000',
  },
  jwt: {
    secretOrKey: 'secret',
    expiresIn: 36000000,
  },
  mail: {
    host: 'mail.ethionet.et',
    port: 25,
    secure: false,
    user: 'info@purposeblack.et',
    pass: 'info@purpose2022email',
  },
};
