import { Post, Controller, Get, Param, Body } from '@nestjs/common';
import { AnnualSummitService } from './annual-summit.service';
import { CreateRepresentativePerson } from './dto/create.representative.dto';

@Controller('annual-summit')
export class AnnualSummitController {
  constructor(private annualService: AnnualSummitService) {}

  @Post('create')
  async createNew(@Body() _newPerson: CreateRepresentativePerson) {
    return await this.annualService.createNewRepresentativePerson(_newPerson);
  }

  @Get('all')
  async getAllData() {
    return await this.annualService.getAll();
  }

  @Get(':id')
  async getData() {
    return await this.annualService.getAll();
  }
}
