import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateRepresentativePerson {
  @IsString()
  firstName: string;

  @IsString()
  middelName: string;

  @IsString()
  @IsOptional()
  lastName: string;

  @IsString()
  address: string;

  @IsString()
  city: string;

  @IsString()
  country: string;

  @IsString()
  houseno: string;

  @IsString()
  RfirstName: string;

  @IsString()
  @IsOptional()
  RmiddelName: string;

  @IsString()
  @IsNotEmpty()
  RlastName: string;

  @IsString()
  Raddress: string;

  @IsString()
  Rcity: string;

  @IsString()
  Rcountry: string;

  @IsString()
  Rhouseno: string;

  @IsNumber()
  // @IsNotEmpty()
  share_no: number;

  @IsNumber()
  share_amount: number;

  @IsString()
  share_date: string;
}
