import { Document } from 'mongoose';
import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';

export type EmailStatusDocument = EmailStatus & Document;

@Schema()
export class EmailStatus {
  @Prop()
  type: string;

  @Prop()
  email: string;

  @Prop({ default: false })
  isDelivered: boolean;

  @Prop()
  delivedDate: string;
}

export const EmailStatusSchema = SchemaFactory.createForClass(EmailStatus);
EmailStatusSchema.set('timestamps', true);
