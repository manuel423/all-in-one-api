import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  OrdinaryInvestorsCertficate,
  OrdinaryInvestorsDocument,
} from './schema/Ordinary.schema';
import { Model } from 'mongoose';
import { CreateInvestorsDto } from '../dto/create-investors.dto';
import { ShareTypeEnum } from '../ShareTypeEnum';
import axios from 'axios';
import { FilterInvestorDto } from '../dto/filter.dto';
import { UpdateInvestorDto } from '../dto/update-investors.dto';

let filterNameEng = ['Ato', 'W/o', 'W/t'];
let filterNameAmh = ['አቶ', 'ወ/ሮ', 'ወ/ት'];

@Injectable()
export class OrdinaryService {
  constructor(
    @InjectModel(OrdinaryInvestorsCertficate.name)
    private investorModel: Model<OrdinaryInvestorsDocument>,
  ) {}

  async stasticOrdinaryInvestorsData() {
    let _res = await this.investorModel.find();
    return { total_ordinary: _res.length };
  }

  async findAllInvestors() {
    return await this.investorModel.aggregate([
      {
        $addFields: {
          name: {
            $concat: [
              '$fullName.firstName',
              ' ',
              '$fullName.middleName',
              ' ',
              '$fullName.lastName',
            ],
          },
        },
      },
    ]);
  }

  async findOneShareholder(id) {
    return await this.investorModel.findById(id);
  }

  async filterInvestors(type) {
    let result = await this.investorModel.find({ type });
    if (!result) {
      return { message: 'Data not found', status: false };
    }
    if (result.length === 0) {
      return { message: `No ${type} Data found`, status: true, data: result };
    }
    return { message: 'Data Fetching...', status: true, data: result };
  }

  async createInvestors(createDto: CreateInvestorsDto) {
    console.log('creating.....');
    return await this.investorModel.create(createDto);
  }

  async updateOrdinaryById(id, _updateInvestorDto: UpdateInvestorDto) {
    let r = this.findOneShareholder(id);
    if (r) {
      return this.investorModel
        .findByIdAndUpdate(id, _updateInvestorDto)
        .then(() => {
          return { message: `${id} is modified successfully!` };
        });
    }
  }

  /**
   * @description stastic report for dashboard
   * @returns
   */
  async dashboardStatic() {
    let _dataFranchise = (
      await this.investorModel.find({ type: ShareTypeEnum.franchise })
    ).length;
    let _dataKegeberew = (
      await this.investorModel.find({ type: ShareTypeEnum['kegeberew tower'] })
    ).length;
    let _dataKipt = (
      await this.investorModel.find({ type: ShareTypeEnum.kipt })
    ).length;
    let _dataOrdinary = (
      await this.investorModel.find({ type: ShareTypeEnum.ordinary })
    ).length;
    let _dataOthers = (
      await this.investorModel.find({ type: ShareTypeEnum.others })
    ).length;

    return {
      total_franchise: _dataFranchise,
      total_kegeberew: _dataKegeberew,
      total_kipt: _dataKipt,
      total_ordinary: _dataOrdinary,
      total_others: _dataOthers,
    };
  }

  async dashboardStaticPercentage() {
    let _total = (await this.investorModel.find()).length;
    let _dataFranchise = (
      await this.investorModel.find({ type: ShareTypeEnum.franchise })
    ).length;
    let _dataKegeberew = (
      await this.investorModel.find({ type: ShareTypeEnum['kegeberew tower'] })
    ).length;
    let _dataKipt = (
      await this.investorModel.find({ type: ShareTypeEnum.kipt })
    ).length;
    let _dataOrdinary = (
      await this.investorModel.find({ type: ShareTypeEnum.ordinary })
    ).length;
    let _dataOthers = (
      await this.investorModel.find({ type: ShareTypeEnum.others })
    ).length;

    let _res = {
      franchise: (_dataFranchise / _total) * 100,
      kegeberew: (_dataKegeberew / _total) * 100,
      kipt: (_dataKipt / _total) * 100,
      ordinary: (_dataOrdinary / _total) * 100,
      others: (_dataOthers / _total) * 100,
    };

    return {
      data: Object.values(_res),
      label: Object.keys(_res),
    };
  }

  async getExcelData(data) {
    console.log('data');
    // return await axios
    //   .get('http://192.168.1.105:3000/')
    //   .then(async (res) => {
    console.log(data);
    // return res.data;
    return await data.forEach(async (element) => {
      const _cre: CreateInvestorsDto = {
        serialNumber: element['serialNo'],
        accountNumber: element['accountNo'],
        fullName: {
          firstName: element['fullName']?.split(' ')[0],
          middleName: element['fullName']?.split(' ')[1],
          lastName: element['fullName']?.split(' ')[2],
          title: '',
        },
        fullNameAmharic: {
          amharicFirstName: element['fullNameAmharic']
            ? element['fullNameAmharic']?.split(' ')[0]
            : '',
          amharicMiddleName: element['fullNameAmharic']
            ? element['fullNameAmharic']?.split(' ')[1]
            : '',
          amharicLastName: element['fullNameAmharic']
            ? element['fullNameAmharic']?.split(' ')[2]
            : '',
          title: '',
        },
        numberofShares: element['numberOfShare'],
        amountPaid: element['totalShareAmountBirrPaid'],
        from: element['from'],
        to: element['to'],
        certficateNumber: element['certficateNumber'],
        telephone: element['telephone'],
        email: element['email'] ? element['email'] : '',
        status: 'ISSUED',
        type: 'ORDINARY SHARE',
        remark: '',
        sheetName: element['sheetName'],
        enumName: '',
        location: '',
        moneyToWordEng: '',
        moneyToWordETH: '',
        dateFormatEng: '',
        dateFormatETH: '',
      };
      await this.createInvestors(_cre);
    });
    // })
    // .catch((err) => {
    //   console.log(err);
    //   return { message: 'Something is wrong! try again', status: false };
    // });
  }

  /**
   * @description generate Certficate ID for newly to created data
   */
  async generateCertifcateID() {
    let r = await this.investorModel.find();
    let _result = {};
    if (r.length > 0) {
      let _lastData = await this.investorModel
        .find({}, {}, { sort: { certficateNumber: -1 } })
        .limit(1);
      _result['data'] = _lastData;
      _result['next_certficate_no'] = {
        accountNumber: Number(_lastData[0].accountNumber) + 1,
        serialNumber: _lastData[0].serialNumber + 1,
        certficateNumber: Number(_lastData[0].certficateNumber) + 1,
        startFrom: _lastData[0].to + 1,
      };
    } else {
      _result['data'] = [];
      _result['next_certficate_no'] = {
        accountNumber: 1,
        serialNumber: 1,
        certficateNumber: '0001',
        startFrom: 0,
      };
    }

    return _result;
  }

  async filterOrdinary(_filterInvestor: FilterInvestorDto) {
    let { accountNumber, email, name, serialNumber } = _filterInvestor;

    if (accountNumber) {
      return this.investorModel.find({ accountNumber });
    }
    if (serialNumber) {
      return this.investorModel.find({ serialNumber });
    }
    if (email) {
      return this.investorModel.find({ email });
    }
    if (accountNumber) {
      this.investorModel.find({ accountNumber });
    }
  }
}
