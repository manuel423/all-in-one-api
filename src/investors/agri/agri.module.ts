import { Module } from '@nestjs/common';
import { AgriService } from './agri.service';
import { MongooseModule } from '@nestjs/mongoose';
import {
  AgriInvestorsCertficate,
  AgriInvestorsCertficateSchema,
} from './schema/AgriInvestors.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: AgriInvestorsCertficate.name,
        schema: AgriInvestorsCertficateSchema,
      },
    ]),
  ],
  providers: [AgriService],
  exports: [AgriService],
})
export class AgriModule {}
