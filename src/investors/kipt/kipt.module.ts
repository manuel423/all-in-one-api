import { Module } from '@nestjs/common';
import { KiptService } from './kipt.service';
import { MongooseModule } from '@nestjs/mongoose';
import {
  KiptCertficateSchema,
  KiptInvestorsCertficate,
} from './schema/kipt.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: KiptInvestorsCertficate.name, schema: KiptCertficateSchema },
    ]),
  ],
  providers: [KiptService],
  exports: [KiptService]
})
export class KiptModule {}
