import { Injectable } from '@nestjs/common';
import * as path from 'path';
import * as csvWriter from 'csv-writer';
import { OrdinaryService } from '../ordinary/ordinary.service';
// const csvWriter = require('csv-writer');

@Injectable()
export class ReportService {
  constructor(private readonly ordinaryService: OrdinaryService) {}

  async ordinaryExport() {
    const writeer = csvWriter.createObjectCsvWriter({
      path: path.resolve(__dirname, 'orinary_report.csv'),
      header: [
        { id: 'serialNumber', title: 'Serial Number' },
        { id: 'accountNumber', title: 'Account Number' },
        { id: 'firstName', title: 'FirstName' },
        { id: 'lastName', title: 'Last Name' },
        { id: 'numberofShares', title: 'Number of Shares' },
        { id: 'amountPaid', title: 'Amount Paid' },
        { id: 'from', title: 'From' },
        { id: 'to', title: 'To' },
        { id: 'certficateNumber', title: 'Certficate Number' },
        { id: 'type', title: 'Type' },
      ],
    });
    const _data = await this.ordinaryService.findAllInvestors();
    writeer.writeRecords(_data).then(()=>{
        console.log('Export Data Done!');
    })
  }
}
