import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { KegeberewTowerController } from './kegeberewTower.controller';
import { KegeberewTowerService } from './kegeberewTower.service';
import {
  KegeberewTowerPerson,
  KegeberewTowerSchema,
} from './schema/kegeberewtower.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: KegeberewTowerPerson.name, schema: KegeberewTowerSchema },
    ]),
  ],
  controllers: [KegeberewTowerController],
  providers: [KegeberewTowerService],
})
export class kegeberewTowerModule { }
