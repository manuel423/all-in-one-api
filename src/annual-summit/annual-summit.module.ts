import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AnnualSummitController } from './annual-summit.controller';
import { AnnualSummitService } from './annual-summit.service';
import {
  RepresentativePerson,
  RepresentativeSchema,
} from './schema/representative.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: RepresentativePerson.name, schema: RepresentativeSchema },
    ]),
  ],
  controllers: [AnnualSummitController],
  providers: [AnnualSummitService],
})
export class AnnualSummitModule {}
