/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, Document } from 'mongoose';
// import { ShareTypeEnum } from 'src/investors/ShareTypeEnum';
import { SharehoderType } from '../ShareholderTypes'

export type ShareholderDocument = Document & Shareholder;


@Schema()
export class Shareholder {
  @Prop()
  fullName: string;
  @Prop()
  gender: string;
  @Prop()
  email: string;
  @Prop()
  phoneNumber: string;
  @Prop({ required: false })
  optionalPhone: [string];
  @Prop()
  premiumCollected:number
  @Prop()
  number0fshares: number;
  @Prop()
  amountSubscribed: number;
  @Prop()
  amountPaidincash: number;
  @Prop({enum:SharehoderType , default:SharehoderType['ordinary']})
  type: string;
  @Prop({type:Date,default:new Date})
  regestrationdate:Date;
  @Prop({default:false})
status:boolean;
@Prop()
sequesnce_num:string;
}

export const ShareholderSchema = SchemaFactory.createForClass(Shareholder);
ShareholderSchema.set('timestamps', true);
