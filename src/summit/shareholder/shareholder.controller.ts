/* eslint-disable prettier/prettier */
/* eslint-disable no-var */
/* eslint-disable prettier/prettier */
import {
  Body,
  Param,
  Controller,
  Get,
  Post,
  UsePipes,
  ValidationPipe,
  Put,
  Delete,
  Res,
  Headers,
} from '@nestjs/common';
import { ShareholderService } from './shareholder.service';
// import { LogService} from '../log/log.service'
import { CreateShareholderDto } from './dto/create-shareholder.dto';
import { UpdateShareholderDto } from './dto/update-shareholder.dto';

// import { FilteruserDto } from './dto/filter-user.dto'
import { verify } from 'jsonwebtoken';
import { Header, Query } from '@nestjs/common/decorators';
@Controller('shareholder')
export class ShareholderController {
  constructor(
    private shareholderervice: ShareholderService, // private logservice: LogService,
  ) {}
  @Post('/create')
  // @UsePipes(ValidationPipe)
  async createShareholder(@Body() _User: CreateShareholderDto) {
    return await this.shareholderervice.createshareholder(_User);
  }
  // @Put(':id')
  // async updateUser(
  //   @Param('id') id: string,
  //   @Body() _updateUser: UpdateShareholderDto,
  //   @Headers() Authorization
  // ) {
  //   console.log(Authorization.authorization);
  //   const local = Authorization.authorization
  //   const decode = verify(local,'1234567890')
  //   // console.log(decode);
  //   const userid = decode.userId
  //   const log = {userid:userid,shareholdername:"",shareholderid:id,type:'update'}

  //   var logs = await this.logservice.createshareholder(log);
  //   if(logs){
  //     return this.shareholderervice.updateuser(id, _updateUser);
  //   }
  //   else{
  //     console.log('no')
  //   }

  // }
  @Get('/all')
  async getall(@Query('limit') limit: number, @Query('skip') skip: number) {
    return await this.shareholderervice.getall(skip, limit);
  }
  @Get('/notifi')
  async report() {
    return await this.shareholderervice.filterlength();
  }
  @Get('/:id')
  async getbyid(@Param('id') id: string) {
    return await this.shareholderervice.getuserbyid(id);
  }
  @Get('/sequence/:num')
  async getshareholderbysequence(@Param('num') num) {
    return await this.shareholderervice.getshareholderbysequencenumber(num);
  }

  @Get('from/Excel')
  getFile() {
    return this.shareholderervice.getFileFromExcel();
  }
}
