export enum SharehoderType{
    'founder' = 'FOUNDER',
    'ordinary' = 'ORDINARY',
    'franchise_agriculture' = 'AGRICULTURE',
    'franchise_agroprocessing' = 'AGRO-PROCESSING',
    'franchise_retail' = 'RETAIL',
    'farmer' = 'FARMER',
  }