export enum ShareTypeEnum {
  'ordinary' = 'ORDINARY SHARE',
  'franchise' = 'FRANCHISE SHARE',
  'kegeberew tower' = 'KEGEBEREW TOWER SHARE',
  'kipt' = 'FARMER SHARE',
  'others' = 'Others',
}
