import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { InvestorsController } from './investors.controller';
// import { InvestorsService } from './investors.service';
// import {
//   InvestorsCertficate,
//   InvestorsCertficateSchema,
// } from './agri/schema/Investors.schema';
import { AgriModule } from './agri/agri.module';
import { AgroModule } from './agro/agro.module';
import { OrdinaryModule } from './ordinary/ordinary.module';
import { KiptModule } from './kipt/kipt.module';
import { ReportModule } from './report/report.module';

@Module({
  imports: [
    AgriModule,
    AgroModule,
    OrdinaryModule,
    KiptModule,
    ReportModule,
  ],
  controllers: [InvestorsController],
  providers: [],
})
export class InvestorsModule {}
