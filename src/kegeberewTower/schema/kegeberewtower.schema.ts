import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type KegeberewTowerDocument = Document & KegeberewTowerPerson;

@Schema()
export class KegeberewTowerPerson {
  @Prop()
  first_name: string;

  @Prop()
  middel_name: string;

  @Prop()
  last_name: string;

  @Prop()
  address: string;

  @Prop()
  kebele: string;

  @Prop()
  telephone: string;

  @Prop()
  telephone_alt: string;

  @Prop()
  house_no: string;

  @Prop()
  bank_account: string;

  @Prop()
  Rfirst_name: string;

  @Prop()
  Rmiddel_name: string;

  @Prop()
  Rlast_name: string;

  @Prop()
  Raddress: string;

  @Prop()
  Rkebele: string;

  @Prop()
  Rtelephone: string;

  @Prop()
  Rtelephone_alt: string;

  @Prop()
  Rhouse_no: string;

  @Prop()
  kegeberewTower_type: string;

  @Prop()
  applicant_name: string;

  @Prop()
  share_no: string;

  @Prop()
  share_amount: string;

  @Prop()
  share_date: string;
}

export const KegeberewTowerSchema =
  SchemaFactory.createForClass(KegeberewTowerPerson);
