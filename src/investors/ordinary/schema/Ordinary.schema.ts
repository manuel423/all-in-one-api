import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ShareTypeEnum } from '../../ShareTypeEnum';
import { StatusEnum } from '../../StatusEnum';

class fullNameClass {
  @Prop()
  firstName: string;

  @Prop()
  middleName: string;

  @Prop()
  lastName: string;

  @Prop()
  title: string;
}

class fullNameAmharicClass {
  @Prop()
  amharicFirstName: string;

  @Prop()
  amharicMiddleName: string;

  @Prop()
  amharicLastName: string;

  @Prop()
  title: string;
}

export type OrdinaryInvestorsDocument = OrdinaryInvestorsCertficate & Document;

@Schema({
    collection: 'OrdinaryInvestors'
})
export class OrdinaryInvestorsCertficate {
  @Prop()
  serialNumber: number;

  @Prop()
  accountNumber: string;

  @Prop()
  fullName: fullNameClass;

  @Prop()
  fullNameAmharic: fullNameAmharicClass;

  @Prop()
  numberofShares: number;

  @Prop()
  amountPaid: number;

  @Prop()
  from: number;

  @Prop()
  to: number;

  @Prop()
  certficateNumber: string;

  @Prop()
  telephone: string;

  @Prop()
  email: string;

  @Prop({ required: false, enum: StatusEnum, default: StatusEnum.issued })
  status: string;

  @Prop()
  sheetName: string;

  @Prop({
    required: false,
    enum: ShareTypeEnum,
    default: ShareTypeEnum.ordinary,
  })
  type: string;

  @Prop({ default: null, type: String })
  remark: string;

  @Prop()
  enumName: string;

  @Prop({ required: false, trim: true })
  moneyToWordEng: string;

  @Prop({ required: false, trim: true })
  moneyToWordETH: string;

  @Prop({default: Date.now()})
  dateFormatEng: string;

  @Prop()
  dateFormatETH: string;
}

export const OrdinaryInvestorsCertficateSchema = SchemaFactory.createForClass(
  OrdinaryInvestorsCertficate,
);
OrdinaryInvestorsCertficateSchema.set('timestamps', true);
