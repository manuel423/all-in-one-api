import { Post, Controller, Get, Param, Body } from '@nestjs/common';
import { KegeberewTowerService } from './kegeberewTower.service';
import { CreateKegeberewTowerPerson } from './dto/create.applicationForm.dto';

@Controller('kegeberewTower')
export class KegeberewTowerController {
  constructor(private kegeberewTower: KegeberewTowerService) { }

  @Post('create')
  async createNew(@Body() _newPerson: CreateKegeberewTowerPerson) {
    return await this.kegeberewTower.createNewKegeberewTowerPerson(_newPerson);
  }

  @Get('all')
  async getAllData() {
    return await this.kegeberewTower.getAll();
  }

  @Get(':id')
  async getData() {
    return await this.kegeberewTower.getAll();
  }
}
