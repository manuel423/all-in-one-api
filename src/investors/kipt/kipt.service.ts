import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  KiptInvestorsCertficate,
  KiptInvestorsDocument,
} from './schema/kipt.schema';
import { Model } from 'mongoose';
import axios from 'axios';
import { CreateInvestorsDto } from '../dto/create-investors.dto';
import { ShareTypeEnum } from '../ShareTypeEnum';
import { UpdateInvestorDto } from '../dto/update-investors.dto';
import { FilterInvestorDto } from '../dto/filter.dto';

@Injectable()
export class KiptService {
  constructor(
    @InjectModel(KiptInvestorsCertficate.name)
    private farmerModel: Model<KiptInvestorsDocument>,
  ) {}

  getAllFarmers() {
    return this.farmerModel.aggregate([
      {
        $addFields: {
          name: {
            $concat: [
              '$fullName.firstName',
              ' ',
              '$fullName.middleName',
              ' ',
              '$fullName.lastName',
            ],
          },
        },
      },
    ]);
  }

  async stasticFarmersInvestorsData() {
    let _res = await this.farmerModel.find();
    return { total_farmers: _res.length };
  }

  singleFarmer(id) {
    return this.farmerModel.findById(id);
  }

  async createInvestors(createDto: UpdateInvestorDto) {
    console.log('creating farmers');
    return await this.farmerModel.create(createDto);
  }

  async updateFarmerById(id, _updateInvestorDto: UpdateInvestorDto) {
    let r = this.singleFarmer(id);
    if (r) {
      return this.farmerModel
        .findByIdAndUpdate(id, _updateInvestorDto)
        .then(() => {
          return { message: `${id} is modified successfully!` };
        });
    }
  }

  async getFarmersExcelData() {
    return await axios
      .get('http://localhost:4000/excel/investors/certficate')
      .then(async (res) => {
        console.log(res.data);
        // return res.data;
        await res.data.forEach(async (element) => {
          const _cre: CreateInvestorsDto = {
            serialNumber: element['serialNo'],
            accountNumber: element['accountNo'],
            fullName: {
              firstName: element['fullName']?.split(' ')[0],
              middleName: element['fullName']?.split(' ')[1],
              lastName: element['fullName']?.split(' ')[2],
              title: '',
            },
            fullNameAmharic: {
              amharicFirstName: element['fullNameAmharic']
                ? element['fullNameAmharic']?.split(' ')[0]
                : '',
              amharicMiddleName: element['fullNameAmharic']
                ? element['fullNameAmharic']?.split(' ')[1]
                : '',
              amharicLastName: element['fullNameAmharic']
                ? element['fullNameAmharic']?.split(' ')[2]
                : '',
              title: '',
            },
            numberofShares: element['numberOfShare'],
            amountPaid: element['totalShareAmountBirrPaid'],
            from: element['from'],
            to: element['to'],
            certficateNumber: element['certficateNumber'],
            telephone: element['telephone'],
            email: element['email'] ? element['email'] : '',
            status: 'ISSUED',
            type:
              element['shareType'] === 'FARMER SHARE'
                ? ShareTypeEnum.kipt
                : ShareTypeEnum.kipt,
            remark: '',
            sheetName: element['sheetName'],
            location: element['location'],
            enumName: '',
            moneyToWordEng: '',
            moneyToWordETH: '',
            dateFormatEng: '',
            dateFormatETH: '',
          };
          await this.createInvestors(_cre);
        });
      })
      .catch((err) => {
        // console.log(err);
        return { message: 'Something is wrong! try again', status: false };
      });
  }

  async generateCertifcateID() {
    let r = await this.farmerModel.find();
    let _result = {};
    if (r.length > 0) {
      let _lastData = await this.farmerModel
        .find({}, {}, { sort: { certficateNumber: -1 } })
        .limit(1);
      _result['data'] = _lastData;
      _result['next_certficate_no'] = {
        accountNumber: Number(_lastData[0].accountNumber) + 1,
        serialNumber: _lastData[0].serialNumber
          ? _lastData[0].serialNumber + 1
          : 1,
        certficateNumber: Number(_lastData[0].certficateNumber) + 1,
        startFrom: _lastData[0].to + 1,
      };
    } else {
      _result['data'] = [];
      _result['next_certficate_no'] = {
        accountNumber: 1,
        serialNumber: 1,
        certficateNumber: '0001',
        startFrom: 0,
      };
    }

    return _result;
  }

  async FilterFarmers(_filterInvestor: FilterInvestorDto) {
    let { accountNumber, email, name, serialNumber } = _filterInvestor;

    if (accountNumber) {
      return this.farmerModel.find({ accountNumber });
    }
    if (serialNumber) {
      return this.farmerModel.find({ serialNumber });
    }
    if (email) {
      return this.farmerModel.find({ email });
    }
    if (accountNumber) {
      this.farmerModel.find({ accountNumber });
    }
  }
}
