import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateRepresentativePerson } from './dto/create.representative.dto';
import {
  RepresentativeDocument,
  RepresentativePerson,
} from './schema/representative.schema';

@Injectable()
export class AnnualSummitService {
  constructor(
    @InjectModel(RepresentativePerson.name)
    private representativeModel: Model<RepresentativeDocument>,
  ) { }

  async createNewRepresentativePerson(
    createPerson: CreateRepresentativePerson,
  ) {
    return await this.representativeModel.create(createPerson).then((res) => {
      return { message: 'Representative Created!, Thank you' };
    });
  }

  async getAll() {
    return await this.representativeModel.find();
  }

  async getFindOneRepresentative(id) {
    let r = await this.representativeModel.findById(id);
    if (!r) {
      return { message: 'Not Found', status: false };
    }
    return r;
  }
}
