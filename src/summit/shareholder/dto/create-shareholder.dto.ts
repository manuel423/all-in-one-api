/* eslint-disable prettier/prettier */
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsEmail,
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsObject,
  IsOptional,
  IsPhoneNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Date } from 'mongoose';
import { SharehoderType } from '../ShareholderTypes';
// import { SharehoderType } from '../../../utility/CreditType.enum'

export class CreateShareholderDto {
  @IsString()
  @IsNotEmpty()
  fullName:string;
  @IsString()
  gender: string;
  @IsEmail()
  @IsOptional()
  email: string;
  @IsString()
  @IsNotEmpty()
  phoneNumber: string;
  @IsString()
  @IsOptional()
  optionalPhone: string;
  @IsNumber()
  @IsOptional()
  premiumCollected: number;
  @IsNumber()
  @IsOptional()
  number0fshares: number;
  @IsNumber()
  @IsNotEmpty()
  amountSubscribed: number;
  @IsNumber()
  @IsOptional()
  amountPaidincash: number;
  @IsIn([
    SharehoderType.ordinary,
    SharehoderType.founder,
    SharehoderType.franchise_agriculture,
    SharehoderType.franchise_agroprocessing,
    SharehoderType.franchise_retail,
    SharehoderType.farmer,
  ])
  type: string;
  @IsOptional()
  regestrationdate:Date;
  @IsBoolean()
  status:boolean;
  @IsString()
  @IsOptional()
  sequesnce_num:string;
}
