import axios from 'axios';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  AgroInvestorsCertficate,
  AgroInvestorsDocument,
} from './schema/Agro.schema';
import { Model } from 'mongoose';
import { CreateInvestorsDto } from '../dto/create-investors.dto';
import { FilterInvestorDto } from '../dto/filter.dto';
import { UpdateInvestorDto } from '../dto/update-investors.dto';

@Injectable()
export class AgroService {
  constructor(
    @InjectModel(AgroInvestorsCertficate.name)
    private agroModel: Model<AgroInvestorsDocument>,
  ) {}

  async stasticAgroInvestorsData() {
    let _res = await this.agroModel.find();
    return { total_agro: _res.length };
  }

  async findAAgroInvestors() {
    return await this.agroModel.aggregate([
      {
        $addFields: {
          name: {
            $concat: [
              '$fullName.firstName',
              ' ',
              '$fullName.middleName',
              ' ',
              '$fullName.lastName',
            ],
          },
        },
      },
    ]);
  }

  async findOneAgroShareholder(id) {
    return await this.agroModel.findById(id);
  }

  async createInvestors(createDto: CreateInvestorsDto) {
    console.log('creating.....');
    return await this.agroModel.create(createDto);
  }

  async updateAgroById(id, _updateInvestorDto: UpdateInvestorDto) {
    let r = this.findOneAgroShareholder(id);
    if (r) {
      return this.agroModel
        .findByIdAndUpdate(id, _updateInvestorDto)
        .then(() => {
          return { message: `${id} is modified successfully!` };
        });
    }
  }

  async getAgroExcelData(data) {
    // return await axios
    // .get('http://localhost:4000/excel/investors/certficate')
    // .then(async (res) => {
      console.log(data);
    // return res.data;
    return await data.forEach(async (element) => {
      const _cre: CreateInvestorsDto = {
        serialNumber: element['serialNo'],
        accountNumber: element['accountNo'],
        fullName: {
          firstName: element['fullName']?.split(' ')[0],
          middleName: element['fullName']?.split(' ')[1],
          lastName: element['fullName']?.split(' ')[2],
          title: '',
        },
        fullNameAmharic: {
          amharicFirstName: element['fullNameAmharic']
            ? element['fullNameAmharic']?.split(' ')[0]
            : '',
          amharicMiddleName: element['fullNameAmharic']
            ? element['fullNameAmharic']?.split(' ')[1]
            : '',
          amharicLastName: element['fullNameAmharic']
            ? element['fullNameAmharic']?.split(' ')[2]
            : '',
          title: '',
        },
        numberofShares: element['numberOfShare'],
        amountPaid: element['totalShareAmountBirrPaid'],
        from: element['from'],
        to: element['to'],
        certficateNumber: element['certficateNumber'],
        telephone: element['telephone'],
        email: element['email'] ? element['email'] : '',
        status: 'ISSUED',
        type: 'FRANCHISE SHARE',
        remark: '',
        sheetName: element['sheetName'],
        enumName: '',
        location: '',
        moneyToWordEng: '',
        moneyToWordETH: '',
        dateFormatEng: '',
        dateFormatETH: '',
      };
      await this.createInvestors(_cre);
    });
    // })
    // .catch((err) => {
    //   // console.log(err);
    //   return { message: 'Something is wrong! try again', status: false };
    // });
  }

  /**
   * @description generate Certficate ID for newly to created data
   */
  async generateAgroCertifcateID() {
    let r = await this.agroModel.find();
    let _result = {};
    if (r.length > 0) {
      let _lastData = await this.agroModel
        .find({}, {}, { sort: { certficateNumber: -1 } })
        .limit(1);
      // let _result = {};
      _result['data'] = _lastData;
      _result['next_certficate_no'] = {
        accountNumber: Number(_lastData[0].accountNumber) + 1,
        serialNumber: _lastData[0].serialNumber + 1,
        certficateNumber: Number(_lastData[0].certficateNumber) + 1,
        startFrom: _lastData[0].to + 1,
      };
    } else {
      _result['data'] = [];
      _result['next_certficate_no'] = {
        accountNumber: 1,
        serialNumber: 1,
        certficateNumber: '0001',
        startFrom: 0,
      };
    }

    return _result;
  }

  async filterAgro(_filterInvestor: FilterInvestorDto) {
    let { accountNumber, email, name, serialNumber } = _filterInvestor;

    if (accountNumber) {
      return this.agroModel.find({ accountNumber });
    }
    if (serialNumber) {
      return this.agroModel.find({ serialNumber });
    }
    if (email) {
      return this.agroModel.find({ email });
    }
    if (accountNumber) {
      this.agroModel.find({ accountNumber });
    }
  }
}
