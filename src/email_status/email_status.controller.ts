import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { EmailStatusService } from './email_status.service';
import { CreateEmailStatusDto } from './dto/create-emailstatusdto';

@Controller('email-status')
export class EmailStatusController {
  constructor(private emailStatusService: EmailStatusService) {}

  @Get()
  getEmailStatus() {
    return this.emailStatusService.findAll();
  }

  @Get(':id')
  getEmailStatusById(@Param('id') id: string) {
    return this.emailStatusService.findByID(id);
  }

  @Post('create')
  newEmailStatus(@Body() createEmail: CreateEmailStatusDto) {
    return this.emailStatusService.createEmailStatus(createEmail);
  }
}
