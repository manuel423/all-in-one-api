import { Controller, Get } from '@nestjs/common';

@Controller('pdf')
export class PdfController {
    constructor(){}

    @Get('ordinary')
    generateOrdinaryCertficate(){
        // TODO
        return {message:"Ordinary Certificate endpoint"}
    }
    
    @Get('franchise')
    generateFranchiseCertficate(){
        // TODO
        return {message:"Franchise Certificate endpoint"}
    }
}
