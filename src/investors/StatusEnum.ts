export enum StatusEnum {
  'active' = 'ACTIVE',
  'processing' = 'PROCESSING',
  'issued' = 'ISSUED',
}
