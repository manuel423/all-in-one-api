import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { EmailStatus, EmailStatusDocument } from './schema/EmailStatus.schema';
import { Model } from 'mongoose';
import { CreateEmailStatusDto } from './dto/create-emailstatusdto';

@Injectable()
export class EmailStatusService {
  constructor(
    @InjectModel(EmailStatus.name)
    private emailStatusModel: Model<EmailStatusDocument>,
  ) {}

  async findAll() {
    return await this.emailStatusModel.find();
  }

  async findByID(id) {
    return await this.emailStatusModel.findById(id);
  }

  async createEmailStatus(createStatus: CreateEmailStatusDto) {
    return await this.emailStatusModel.create(createStatus);
  }
}
