import { Injectable } from '@nestjs/common';
import {
  AgriInvestorsCertficate,
  AgriInvestorsDocument,
} from './schema/AgriInvestors.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateInvestorsDto } from '../dto/create-investors.dto';
import axios from 'axios';
import { FilterInvestorDto } from '../dto/filter.dto';
import { UpdateInvestorDto } from '../dto/update-investors.dto';

@Injectable()
export class AgriService {
  constructor(
    @InjectModel(AgriInvestorsCertficate.name)
    private agrirModel: Model<AgriInvestorsDocument>,
  ) {}

  async stasticAAgriInvestorsData() {
    const _res = await this.agrirModel.find();
    return { total_agri: _res.length };
  }

  async findAAgriInvestors() {
    return await this.agrirModel.aggregate([
      {
        $addFields: {
          name: {
            $concat: [
              '$fullName.firstName',
              ' ',
              '$fullName.middleName',
              ' ',
              '$fullName.lastName',
            ],
          },
        },
      },
    ]);
  }

  async findOneAgriShareholder(id) {
    return await this.agrirModel.findById(id);
  }

  async createInvestors(createDto: CreateInvestorsDto) {
    console.log('creating.....');
    return await this.agrirModel.create(createDto);
  }

  async updateAgriById(id, _updateInvestorDto: UpdateInvestorDto) {
    const r = this.findOneAgriShareholder(id);
    if (r) {
      return this.agrirModel
        .findByIdAndUpdate(id, _updateInvestorDto)
        .then(() => {
          return { message: `${id} is modified successfully!` };
        });
    }
  }

  async getAgriExcelData(data) {
    // return await axios
    //   .get('http://localhost:3000/')
    //   .then(async (res) => {
    console.log(data);
    // return res.data;
    return await data.forEach(async (element) => {
      const _cre: CreateInvestorsDto = {
        serialNumber: element['serialNo'],
        accountNumber: element['accountNo'],
        fullName: {
          firstName: element['fullName']?.split(' ')[0],
          middleName: element['fullName']?.split(' ')[1],
          lastName: element['fullName']?.split(' ')[2],
          title: '',
        },
        fullNameAmharic: {
          amharicFirstName: element['fullNameAmharic']
            ? element['fullNameAmharic']?.split(' ')[0]
            : '',
          amharicMiddleName: element['fullNameAmharic']
            ? element['fullNameAmharic']?.split(' ')[1]
            : '',
          amharicLastName: element['fullNameAmharic']
            ? element['fullNameAmharic']?.split(' ')[2]
            : '',
          title: '',
        },
        numberofShares: element['numberOfShare'],
        amountPaid: element['totalShareAmountBirrPaid'],
        from: element['from'],
        to: element['to'],
        certficateNumber: element['certficateNumber'],
        telephone: element['Telephone'],
        email: element['Email'] ? element['Email'] : '',
        status: 'ISSUED',
        type: 'FRANCHISE SHARE',
        remark: '',
        sheetName: element['sheetName'],
        enumName: '',
        location: '',
        moneyToWordEng: '',
        moneyToWordETH: '',
        dateFormatEng: '',
        dateFormatETH: '',
      };
      await this.createInvestors(_cre);
    });
    // })
    // .catch((err) => {
    //   // console.log(err);
    //   return { message: 'Something is wrong! try again', status: false };
    // });
  }

  /**
   * @description generate Certficate ID for newly to created data
   */
  async generateAgriCertifcateID() {
    let e = await this.agrirModel.find();
    let _result = {};
    if (e.length > 0) {
      let _lastData = await this.agrirModel
        .find({}, {}, { sort: { certficateNumber: -1 } })
        .limit(1);
      _result['data'] = _lastData;
      _result['next_certficate_no'] = {
        accountNumber: Number(_lastData[0].accountNumber) + 1,
        serialNumber: _lastData[0].serialNumber + 1,
        certficateNumber: Number(_lastData[0].certficateNumber) + 1,
        startFrom: _lastData[0].to + 1,
      };
    } else {
      _result['data'] = [];
      _result['next_certficate_no'] = {
        accountNumber: 1,
        serialNumber: 1,
        certficateNumber: 1,
        startFrom: 0,
      };
    }

    return _result;
  }

  async filterAgriculture(_filterInvestor: FilterInvestorDto) {
    let { accountNumber, email, name, serialNumber } = _filterInvestor;

    if (accountNumber) {
      return this.agrirModel.find({ accountNumber });
    }
    if (serialNumber) {
      return this.agrirModel.find({ serialNumber });
    }
    if (email) {
      return this.agrirModel.find({ email });
    }
    if (accountNumber) {
      this.agrirModel.find({ accountNumber });
    }
  }
}
