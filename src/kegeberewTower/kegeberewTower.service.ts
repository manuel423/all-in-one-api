import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateKegeberewTowerPerson } from './dto/create.applicationForm.dto';
import {
  KegeberewTowerDocument,
  KegeberewTowerPerson,
} from './schema/kegeberewtower.schema';

@Injectable()
export class KegeberewTowerService {
  constructor(
    @InjectModel(KegeberewTowerPerson.name)
    private representativeModel: Model<KegeberewTowerDocument>,
  ) { }

  async createNewKegeberewTowerPerson(
    createPerson: CreateKegeberewTowerPerson,
  ) {
    return await this.representativeModel.create(createPerson).then((res) => {
      return { message: 'Representative Created!, Thank you' };
    });
  }

  async getAll() {
    return await this.representativeModel.find();
  }

  async getFindOneRepresentative(id) {
    let r = await this.representativeModel.findById(id);
    if (!r) {
      return { message: 'Not Found', status: false };
    }
    return r;
  }
}
