/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { ShareholderService } from './shareholder.service';

import {LogModule} from '../log/log.module'
import { ShareholderController } from './shareholder.controller';
import { MongooseModule } from '@nestjs/mongoose';
import {
 Shareholder,ShareholderSchema
} from './schema/shareholder.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Shareholder.name, schema: ShareholderSchema },
    ]),
    LogModule,
  ],
  providers: [ShareholderService],
  controllers: [ShareholderController],
  exports: [ShareholderService],
  
})
export class ShareholderModule {}
