import { Module } from '@nestjs/common';
import { EmailStatusController } from './email_status.controller';
import { EmailStatusService } from './email_status.service';
import { MongooseModule } from '@nestjs/mongoose';
import { EmailStatus, EmailStatusSchema } from './schema/EmailStatus.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: EmailStatus.name, schema: EmailStatusSchema },
    ]),
  ],
  controllers: [EmailStatusController],
  providers: [EmailStatusService],
})
export class EmailStatusModule {}
