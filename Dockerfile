FROM node:14.19.1

ARG PORT

#mongodb url
ARG MONGODB_URL

ENV PORT=$PORT
ENV MONGODB_URL=$MONGODB_URL


ENV PATH /app/node_modules/.bin:$PATH

COPY package*.json ./
RUN npm install 

COPY . .
RUN  npm run build 
CMD  npm run start:prod -- --port 4343

EXPOSE 4343